(function ($) {

Drupal.behaviors.modalFrameAuth = function() {
  var authform = $('form.modalframe_auth:not(.modalframe_auth-processed)').addClass('modalframe_auth-processed').find('input.form-submit').click(function() {
    var element = this;

    function onSubmitCallback(args) {
      if(args != false){
        $(element).parents('.modalframe_auth-processed').submit();
      }
      
    }

    
    // Build modal frame options.
    var modalOptions = {
      url: "/modalframe_auth/authenticate",
      autoFit: true,
      onSubmit: onSubmitCallback,
      width: 420,
      height: 300,
    };

    if(typeof(Drupal.settings.language) !== 'undefined') {
      var lang = Drupal.settings.language;
      modalOptions['url'] = "/" + lang + modalOptions['url'];
    }
    
    
    // Open the modal frame dialog.
  	Drupal.modalFrame.open(modalOptions);

    // Prevent default action of the link click event.
    return false;
  });
};

})(jQuery);
